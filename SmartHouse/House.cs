﻿namespace SmartHouse
{
    public class House
    {
        Security secure;
        private readonly Security _security;
        private readonly InformationService _informationService;


        public House(Security security)
        {
            secure = security;
            _security = secure;
            var fileDatabase = new FileDatabase();
            _informationService = new InformationService(fileDatabase);
        }

        public House()
        {
            _security = secure;
            var fileDatabase = new FileDatabase();
            _informationService = new InformationService(fileDatabase);
        }

        public void EnableFullLockDown()
        {
            _security.FullLockDown();
        }

        public void LockMainDoor()
        {
            _security.LockFrontDoor();
        }

        public string UnlockMainDoor(decimal securityCode)
        {
            var security = secure;

            return security.UnlockFrontDoor(securityCode);
        }

        public string GetHouseHistory()
        {
            return _informationService.GetInformation();
        }
    }
}