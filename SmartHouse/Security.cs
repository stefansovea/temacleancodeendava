﻿namespace SmartHouse
{
    public class Security
    {
        FileDatabase db { get; set; }

        Security (FileDatabase _db)
        {
            db = _db;
        }
        public void FullLockDown()
        {
            const string message = "Full LockDown enabled!";
            db.Save(message);
        }

        public void LockFrontDoor()
        {
            const string message = "Front door locked";
            db.Save(message);
        }

        public string UnlockFrontDoor(decimal accessCode)
        {
            const string validMessage = "Access granted!";
            const string invalidMessage = "Access code Invalid! Access denied!";
            if (accessCode != db.GetAccessCode())
            {
                db.Save(invalidMessage);
                return invalidMessage;
            }

            db.Save(validMessage);

            return validMessage;
        }
    }
}