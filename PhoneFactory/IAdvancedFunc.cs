﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneFactory
{
    interface IAdvancedFunc
    {
        void AccessInternet();
        void ShareFileOverBluetooth(string fileName);
        void ConnectToDeviceViaNFC(string deviceName);
    }
}
