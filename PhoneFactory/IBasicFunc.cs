﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneFactory
{
    interface IBasicFunc
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        void MakeCall(decimal number);
        void PlayMP3(string songName);
        void PowerOn();
        void PowerOff();
        string GetInformation();
    }
}
