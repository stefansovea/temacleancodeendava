﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserCreation
{
    static class Create
    {
        public static Person CreateUser(){
            
            var user = new Person();
            Console.WriteLine("Enter your first name:");
            user.FirstName = Console.ReadLine();
            Console.WriteLine("Enter your last name:");
            user.LastName = Console.ReadLine();
            return user;
        }
    }
}
