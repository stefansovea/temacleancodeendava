﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserCreation
{
    static class Validate
    {
        public static void ValidateUser(Person user)
        {
            if (string.IsNullOrWhiteSpace(user.FirstName))
            {
                Console.WriteLine("First name is not valid!");
                Console.ReadLine();
                return;
            }

            if (string.IsNullOrWhiteSpace(user.LastName))
            {
                Console.WriteLine("Last name is not valid!");
                Console.ReadLine();
                return;
            }
        }
    }
}
