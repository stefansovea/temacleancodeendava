﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserCreation
{
    static class Print
    {
        public static void AfisareUser(Person user)
        {
            Console.WriteLine($"Your new username is {user.FirstName.Substring(0, 1)}{user.LastName}");
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }
    }
}
