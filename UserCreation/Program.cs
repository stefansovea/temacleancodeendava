﻿using System;

namespace UserCreation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to user creation app!");
            var user = Create.CreateUser();
            Validate.ValidateUser(user);
            Print.AfisareUser(user);
        }
    }
}
